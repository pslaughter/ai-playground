import { SudokuProblem } from "./types.ts";

const H = "-";
const H_THICK = "=";
const V = "|";
const V_THICK = "‖";

function getHorizontalSeparator(
  problem: SudokuProblem,
  isThick: boolean
): string {
  const char = isThick ? H_THICK : H;

  return Array(problem.columns).fill(` ${char}${char}${char}`).join("");
}

function getRow(problem: SudokuProblem, rowIdx: number): string {
  const row = Array(problem.columns)
    .fill("")
    .map((_, colIdx) => {
      const isThick = colIdx % problem.squareSize === 0;
      const char = isThick ? V_THICK : V;
      const num = problem.board[rowIdx]?.[colIdx] || " ";
      return `${char} ${num} `;
    })
    .join("");

  return `${row}${V_THICK}`;
}

export function formatProblem(problem: SudokuProblem): string {
  const lines = [];

  for (let i = 0; i < problem.rows; i++) {
    const isThickSeparator = i % problem.squareSize === 0;

    lines.push(getHorizontalSeparator(problem, isThickSeparator));
    lines.push(getRow(problem, i));
  }

  lines.push(getHorizontalSeparator(problem, true));

  return lines.join("\n");
}
