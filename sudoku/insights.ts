import { SudokuProblem } from "./types.ts";
import { SudokuPosition } from "./types.ts";

const throwIfPositionOutOfBounds = (
  position: SudokuPosition,
  problem: SudokuProblem
): void => {
  if (position.colIdx >= problem.columns) {
    throw new Error(
      `Expected position.colIdx (${position.colIdx}) less than problem.columns (${problem.columns}).`
    );
  }
  if (position.rowIdx >= problem.rows) {
    throw new Error(
      `Expected position.rowIdx (${position.rowIdx}) less than problem.rows (${problem.rows}).`
    );
  }
};

export const getConnectedPositions = (
  position: SudokuPosition,
  problem: SudokuProblem
): SudokuPosition[] => {
  throwIfPositionOutOfBounds(position, problem);

  const connectedPositions: SudokuPosition[] = [position];

  // Add all in the same column
  for (let i = 0; i < problem.columns; i++) {
    if (i === position.colIdx) {
      continue;
    }

    connectedPositions.push({
      colIdx: i,
      rowIdx: position.rowIdx,
    });
  }

  // Add all in the same row
  for (let i = 0; i < problem.rows; i++) {
    if (i === position.rowIdx) {
      continue;
    }

    connectedPositions.push({
      colIdx: position.colIdx,
      rowIdx: i,
    });
  }

  // Add all in the square
  const squareStartPosition: SudokuPosition = {
    colIdx:
      Math.floor(position.colIdx / problem.squareSize) * problem.squareSize,
    rowIdx:
      Math.floor(position.rowIdx / problem.squareSize) * problem.squareSize,
  };
  for (let i = 0; i < problem.squareSize; i++) {
    const colIdx = squareStartPosition.colIdx + i;

    if (colIdx === position.colIdx) {
      continue;
    }

    for (let j = 0; j < problem.squareSize; j++) {
      const rowIdx = squareStartPosition.rowIdx + j;

      if (rowIdx === position.rowIdx) {
        continue;
      }

      connectedPositions.push({
        colIdx,
        rowIdx,
      });
    }
  }

  return connectedPositions;
};

export const getPossibleValues = (
  position: SudokuPosition,
  assignedBoard: SudokuProblem["board"],
  problem: SudokuProblem
): string[] => {
  throwIfPositionOutOfBounds(position, problem);

  const values = new Set(["1", "2", "3", "4", "5", "6", "7", "8", "9"]);

  getConnectedPositions(position, problem).forEach(({ rowIdx, colIdx }) => {
    const val = assignedBoard[rowIdx][colIdx] || "";

    if (val) {
      values.delete(val);
    }
  });

  return Array.from(values.values());
};
