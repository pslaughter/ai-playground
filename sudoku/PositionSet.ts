import { SudokuPosition } from "./types.ts";

function toKey(position: SudokuPosition): string {
  return `${position.rowIdx},${position.colIdx}`;
}

export class PositionSet {
  private readonly _set: Set<string>;

  constructor(positions: SudokuPosition[]) {
    this._set = new Set(positions.map(toKey));
  }

  has(position: SudokuPosition) {
    return this._set.has(toKey(position));
  }
}
