import { parseProblem } from "./parseProblem.ts";
import type { SudokuProblem } from "./types.ts";

async function readFile(path: string): Promise<string> {
  try {
    const buffer = await Deno.readFile(path);

    const decoder = new TextDecoder();

    return decoder.decode(buffer);
  } catch (e) {
    throw new Error(`Could not read file at: ${path}`, e);
  }
}

export async function readProblemFile(path: string): Promise<SudokuProblem> {
  const content = await readFile(path);

  const problem = parseProblem(content);

  return problem;
}
