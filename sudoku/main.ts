import { createInitialAssignment } from "./createInitialAssignment.ts";
import { formatProblem } from "./formatProblem.ts";
import { getRemainingPositions } from "./getRemainingPositions.ts";
import { getConnectedPositions } from "./insights.ts";
import { isValidValue } from "./isValidValue.ts";
import { readProblemFile } from "./readProblemFile.ts";
import {
  SudokuAssignment,
  SudokuPositionMetadata,
  SudokuProblem,
} from "./types.ts";

const DEV_INFO = {
  ticks: 0,
};

function printUsage() {
  console.log(`
To run this script, please provide a file path containing a sudoku problem:

  deno run sudoku/main.ts ./sudoku/problem1.txt

The sudoku problem should be in the following format:

  53--7----
  6--195---
  -98----6-
  8---6---3
  4--8-3--1
  7---2---6
  -6----28-
  ---419--5
  ----8--79
`);
}

function isComplete(assignment: SudokuAssignment) {
  return assignment.remainingPositions.length === 0;
}

function popUnassignedPosition(
  assignment: SudokuAssignment
): SudokuPositionMetadata {
  if (assignment.remainingPositions.length === 0) {
    throw new Error("No more positions left!");
  }

  // TODO: Use "most-constrained-variable"
  return assignment.remainingPositions.pop()!;
}

function getOrderedPossibleValues(
  assignment: SudokuAssignment,
  position: SudokuPositionMetadata
) {
  // TODO: Use "least-constraining-value"
  const valuesSet = position.remainingValues.values();

  return valuesSet ? [...valuesSet] : [];
}

function addToAssignment(
  position: SudokuPositionMetadata,
  value: string,
  assignment: SudokuAssignment,
  problem: SudokuProblem
) {
  assignment.board[position.rowIdx][position.colIdx] = value;

  const connectedPositions = getConnectedPositions(position, problem);
  getRemainingPositions(assignment, connectedPositions).forEach(
    (remainingPosition) => {
      remainingPosition.remainingValues.delete(value);
    }
  );
}

function removeFromAssignment(
  position: SudokuPositionMetadata,
  value: string,
  assignment: SudokuAssignment,
  problem: SudokuProblem
) {
  assignment.board[position.rowIdx][position.colIdx] = "";

  const connectedPositions = getConnectedPositions(position, problem);
  getRemainingPositions(assignment, connectedPositions).forEach(
    (remainingPosition) => {
      // Reset possible values. TODO: We could be a bit more optimized about this...
      const isValid = isValidValue(
        remainingPosition,
        { ...problem, board: assignment.board },
        value
      );

      if (isValid) {
        remainingPosition.remainingValues.add(value);
      }
    }
  );
}

function backtrackingSearch(problem: SudokuProblem) {
  return backtrack(createInitialAssignment(problem), problem);
}

function backtrack(
  assignment: SudokuAssignment,
  problem: SudokuProblem
): SudokuAssignment | "failure" {
  if (isComplete(assignment)) {
    return assignment;
  }

  // TODO: Pop and getUnassigned should probably be 2 different operations
  const position = popUnassignedPosition(assignment);
  const values = getOrderedPossibleValues(assignment, position);
  for (let i = 0; i < values.length; i++) {
    DEV_INFO.ticks += 1;

    const value = values[i];
    addToAssignment(position, value, assignment, problem);
    const result = backtrack(assignment, problem);

    // TODO: We probably shouldn't use a magic string here...
    if (result !== "failure") {
      return result;
    }

    removeFromAssignment(position, value, assignment, problem);
  }
  // NB: Add back to remainingPositions so that we'll evaluate this again if necessary
  assignment.remainingPositions.push(position);

  return "failure";
}

async function main(): Promise<void> {
  const path = Deno.args[0];

  if (!path) {
    printUsage();
    Deno.exit(1);
    return;
  }

  const problem = await readProblemFile(path);
  const assignment = await backtrackingSearch(problem);

  console.log(`[Debug] Ran in ${DEV_INFO.ticks} ticks...`);

  if (assignment === "failure") {
    console.log("FAIL! No solutions for:");
    console.log(formatProblem(problem));
    Deno.exit(1);
  }

  console.log("SUCCESS! Found solution:");
  console.log(
    formatProblem({
      ...problem,
      board: assignment.board,
    })
  );
}

main();
