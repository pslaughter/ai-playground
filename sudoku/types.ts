export interface SudokuProblem {
  readonly columns: number;
  readonly rows: number;
  readonly squareSize: number;
  readonly board: string[][];
}

export interface SudokuPosition {
  readonly rowIdx: number;
  readonly colIdx: number;
}

export interface SudokuPositionMetadata extends SudokuPosition {
  readonly remainingValues: Set<string>;
}

export interface SudokuAssignment {
  remainingPositions: SudokuPositionMetadata[];
  board: string[][];
}
