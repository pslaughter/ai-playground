import { SudokuProblem } from "./types.ts";

const COLUMNS = 9;
const ROWS = 9;
const SQUARE_SIZE = 3;
const ALLOWABLE_CHARS: ReadonlySet<string> = new Set([
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
]);
const EMPTY_CHARS: ReadonlySet<string> = new Set(["-", " "]);

export function parseProblem(content: string): SudokuProblem {
  const board = Array(ROWS)
    .fill(1)
    .map((_) => Array(COLUMNS).fill(""));

  content.split("\n").forEach((line, rowIdx) => {
    if (rowIdx >= ROWS && line) {
      throw new Error(
        `Unexpected number of rows at row ${rowIdx + 1}. Expected ${ROWS}.`
      );
    }

    line.split("").forEach((char, colIdx) => {
      if (EMPTY_CHARS.has(char)) {
        return;
      }
      if (colIdx >= COLUMNS) {
        throw new Error(
          `Unexpected number of columns at row ${
            rowIdx + 1
          }. Expected ${COLUMNS}.`
        );
      }
      if (ALLOWABLE_CHARS.has(char)) {
        board[rowIdx][colIdx] = char;
      }
    });
  });

  return {
    board,
    columns: COLUMNS,
    rows: ROWS,
    squareSize: SQUARE_SIZE,
  };
}
