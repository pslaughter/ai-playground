import { getConnectedPositions } from "./insights.ts";
import { SudokuPosition, SudokuProblem } from "./types.ts";

export function isValidValue(
  position: SudokuPosition,
  problem: SudokuProblem,
  value: string
) {
  return getConnectedPositions(position, problem).every(
    ({ rowIdx, colIdx }) => {
      if (position.rowIdx === rowIdx && position.colIdx === colIdx) {
        return true;
      }

      return problem.board[rowIdx][colIdx] !== value;
    }
  );
}
