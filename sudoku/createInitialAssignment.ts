import { getPossibleValues } from "./insights.ts";
import {
  SudokuAssignment,
  SudokuPosition,
  SudokuPositionMetadata,
  SudokuProblem,
} from "./types.ts";

export function createInitialAssignment(
  problem: SudokuProblem
): SudokuAssignment {
  const remainingPositions: SudokuPositionMetadata[] = [];

  for (let i = 0; i < problem.rows; i++) {
    for (let j = 0; j < problem.columns; j++) {
      const val = problem.board[i][j];

      if (!val) {
        const emptyPosition: SudokuPosition = {
          rowIdx: i,
          colIdx: j,
        };

        const values = getPossibleValues(emptyPosition, problem.board, problem);
        remainingPositions.push({
          ...emptyPosition,
          remainingValues: new Set(values),
        });
      }
    }
  }

  return {
    remainingPositions,
    // Clone matrix
    board: problem.board.map((x) => x.map((y) => y)),
  };
}
