import { PositionSet } from "./PositionSet.ts";
import {
  SudokuAssignment,
  SudokuPosition,
  SudokuPositionMetadata,
} from "./types.ts";

export function getRemainingPositions(
  assignment: SudokuAssignment,
  positions?: SudokuPosition[]
): SudokuPositionMetadata[] {
  const remainingPositions = assignment.remainingPositions;

  if (!positions) {
    return remainingPositions;
  }

  const positionSet = new PositionSet(positions);

  return remainingPositions.filter((position) => positionSet.has(position));
}
